/* eslint-disable no-use-before-define,no-unused-vars,prefer-destructuring,no-param-reassign,no-prototype-builtins,max-len,no-useless-escape */
// noinspection RegExpRedundantEscape
/**
 *
 * String Tokenizer functions
 *
 * Substitutes parameters inside strings
 * Parameters are separated by $ at the beggining and end: $PARAM$, $PARAM1$
 * Parameters can navigate source object like $object/very/deep$
 * Parameters can include VERY SIMPLE default values like $object/very/deep=myvalue$
 *
 *          Note that default values cannot contain conflicting regexp symbols. This is a side functionality
 *          and not meant to support complex use cases, but just provide simple default values with alpha chars
 *          and numbers
 *
 * (c) Rodolfo LP 2001 - Public Domain
 *
 * @typedef {{ encode?:boolean, unescape?:boolean, dothrow?:boolean, defvalue?: String|Number }} TokenizeOptions
 *
 */

// eslint-disable-next-line no-useless-escape
const

	TOK_EXPR = /\$([A-Za-z0-9_\/\:\=]+)\$/g,

	Parsers = {
		//	daterel: epoch => (epoch ? ("" + epoch).timify() : "just now!")
	};

/**
 * Tokenizes a string with an object.
 * @param {String} str String with templates to tokenize
 * @param {Object} obj Arbitrary tokener object with values~
 * @param {TokenizeOptions} options Tokenizer Options
 * @return {string|*}
 */

function tokenizeCore(str, obj, options) {

	if (!str) return "";
	if (typeof (str) !== "string") return str;

	// eslint-disable-next-line no-param-reassign
	options = options || {};
	obj = obj || {};

	let ckey;

	return str.replace(TOK_EXPR, (match, complexkey) => {

		let defval = null,
			isFreak = false;

		if (complexkey.indexOf("=") > 0) {
			ckey = complexkey.split("=");
			complexkey = ckey[0];
			isFreak = (ckey.length === 3);
			// eslint-disable-next-line no-nested-ternary
			defval = isFreak
				? (options.dothrow ? ckey[2] : null)
				: ckey[1];
		}

		const typed = complexkey.split(":"),
			key = typed[0],
			keytype = typed[1] || null,

			keys = key.split("/"),
			root = keys.shift();

		let
			data = obj[root];

		if (typeof (data) === "number") data = "" + data;

		let fdata = (keys.length ? getPointedData(data, keys.join("/")) : data) || defval;

		if (fdata == null && options.dothrow) {
			throw new Error("Parameter " + root + (keys.length ? "/" + keys.join("/") : "") + " is required");
		}

		if (fdata == null) {
			fdata = options.hasOwnProperty("defvalue")
				? options.defvalue
				: "$" + key + (isFreak ? "==" + ckey[2] : "") + "$";
		}

		if (keytype && Parsers[keytype]) {
			fdata = Parsers[keytype](fdata);
		}

		if (options.unescape) {
			fdata = unescape(fdata);
		}

		return (options.encode ? escape(fdata) : fdata);

	});
}

/**
 * Resolves a pointer into a JSON data structure
 * @param {Object} data JSON Data
 * @param {String} pointer Pointer expression
 * @return {Object | null} resolved data
 */

const getPointedData = function (data, pointer) {
	if (!pointer) return data;
	try {
		const ps = pointer.split("/");
		let d = data;
		// eslint-disable-next-line guard-for-in
		for (const l in ps) {
			d = d[ps[l]];
			if (!d) {
				d = null;
				break;
			}
		}
		return d;
	} catch (e) {
		return null;
	}
};

/**
 * Tokenizes one object with another
 *
 * @param {object} source
 * @param {object} tokenizeValues
 * @param {boolean} strict Whether to require all templates to be resolved
 * @param {boolean} asNewObject Whether to return a new object rather than tokenize the source
 * @retuen {object} source or a new Object
 */

function tokenizeObject(source, tokenizeValues, strict, asNewObject) {

	const tokenizer = strict ? tokenizeStrict : tokenizeKeep;
	const retObject = asNewObject ? {} : source;

	if (typeof source === "string") {
		return tokenizer(source, tokenizeValues);
	}

	if (typeof source === "object") {

		if (source.length) {
			// root is an array
			// an array
			return source.map(item => tokenizeObject(item, tokenizeValues, strict, true));

		}

		return Object.keys(source)
			.reduce((obj, prop, currIdx) => {
				const value = source[prop];
				if (typeof value === "object") {
					obj[prop] = tokenizeObject(value, tokenizeValues, strict, true);
				} else if (typeof value === "string") {
					obj[prop] = tokenizer(source[prop], tokenizeValues);
				} else {
					obj[prop] = value;
				}
				return obj;
			}, retObject);
	}

	return retObject;
}

const

	/**
	 * Convenience: Tokenizes a String with an object in Keep Mode: Missing values will not throw an error, but just leave the template as-is. This
	 * is useful for a multi-stage tokenization when some input values are not available on every stage.
	 *
	 * @param {String} str String with templates to tokenize
	 * @param {Object} obj Arbitrary tokener object with values
	 * @param {boolean} isEncode Whether to URL-Encode property values
	 * @return {String} The tokenized string
	 */

	tokenizeKeep = (str, obj, isEncode) => tokenizeCore(str, obj, {encode: isEncode}),

	/**
	 * Convenience: Tokenizes a String with an object and uses the provided default value if parameters are not found.
	 * @param {String} str String with templates to tokenize
	 * @param {Object} obj Arbitrary tokener object with values
	 * @param {String|Number} defaultValue The default value if property is not found in the tokener object
	 * @return {String} The tokenized string
	 */

	tokenizeWithDefault = (str, obj, defaultValue) => tokenizeCore(str, obj, defaultValue == null ? null : {defvalue: defaultValue}),

	/**
	 * Convenience: Tokenizes a string with an object in Strict Mode: Will throw an exception if parameters are missing.
	 * @param {String} str String with templates to tokenize
	 * @param {Object} obj Arbitrary tokener object with values~
	 * @param {boolean} isEncode Whether to URL-Encode property values
	 * @param {boolean} isUnescape Whether to unescape property values
	 * @return {String} The tokenized string
	 */

	tokenizeStrict = (str, obj, isEncode = false, isUnescape = false) => tokenizeCore(str, obj, {
		encode: !!isEncode,
		unescape: !!isUnescape,
		dothrow: true
	});


// noinspection JSUnusedGlobalSymbols
module.exports = {
	tokenizeStrict,
	tokenizeObject,
	tokenizeWithDefault,
	getPointedData
};
