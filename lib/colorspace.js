// Colorspace translation

function getCbCr(color) {
	return {cb: rgb2cb(color), cr: rgb2cr(color)};
}

function rgb2cb(color) {
	/*a utility function to convert colors in RGB into YCbCr*/
	return Math.floor((128 + -0.168736 * color.r - 0.331264 * color.g + 0.5 * color.b));
}

function rgb2cr(color) {
	/*a utility function to convert colors in RGB into YCbCr*/
	return Math.floor((128 + 0.5 * color.r - 0.418688 * color.g - 0.081312 * color.b));
}

/**
 *
 * Compare colors in CbCr colorspace
 *
 * @param Cb_p	Cb of pixel
 * @param Cr_p Cr of pixel
 * @param Cb_key Cb of another pixel
 * @param Cr_key Cr of another pixel
 * @param tola A tolerance
 * @param tolb B tolerance
 * @return {number} Distance: 0: no, 0...1 percent to TOLB, 1 yes
 */

function colorcloseYuv(Cb_p, Cr_p, Cb_key, Cr_key, tola, tolb) {
	/*decides if a color is close to the specified hue*/
	const temp = Math.sqrt((Cb_key - Cb_p) * (Cb_key - Cb_p) + (Cr_key - Cr_p) * (Cr_key - Cr_p));
	if (temp < tola) {
		return (0.0);
	}
	if (temp < tolb) {
		return ((temp - tola) / (tolb - tola));
	}
	return (1.0);
}


/**
 * Compare a RGB color with a CbCr color (for performance as CbCr color will normally be the same)
 * @param color1rgb Color in RGB
 * @param color2yuv Color in YCbCr
 * @param tola Exact tolerance
 * @param tolb Diffuse tolerance
 * @return {boolean} Whether colors are similar according to tolerances
 */

function isColor(color1rgb, color2yuv, tola, tolb) {
	const col1cr = rgb2cr(color1rgb), col1cb = rgb2cb(color1rgb);
	return colorcloseYuv(col1cb, col1cr, color2yuv.cb, color2yuv.cr, tola, tolb) < 0.3;
}


module.exports = {
	isColor,
	colorcloseYuv,
	getCbCr,
	rgb2cb,
	rgb2cr
}
