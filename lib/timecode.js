/* eslint-disable max-len */
/**
 *
 * TimeCode parsing library
 * (C) 2020 Nebular Streams
 *
 * @typedef {{ width:number, height:number, getPixel:(function(number,number,number?)), crop:(function(Object)), writeFile:(function(string)) }} Bitmap
 * @typedef {{ slice?:number, debug?:boolean, SKIP?:number, filename?:string, time?:number, result?:Object, parser?:(function(ScanResult[]):[]), progress?:boolean, CHROMAKEY_ONE?: {cb: number, cr: number}, TOLB?: number, TOLA?: number, CHROMAKEY_FRAME?: {cb: number, cr: number}, CHROMAKEY_ZERO?: {cb: number, cr: number}, errordir?:string}} TimeCodeOptions
 * @typedef {{ time:number, result:number[] }} ScanResult
 */

// noinspection JSUnusedLocalSymbols

const

	TAG = "[timecode]",
	fs = require("fs"),
	imagejs = require("imagejs"),
	colorspace = require("./colorspace"),

	OPTIONS = {

		// will use YCbCr colorspace to better detect pixels
		CHROMAKEY_FRAME: colorspace.getCbCr({
			r: 0,
			g: 255,
			b: 0
		}),
		CHROMAKEY_ONE: colorspace.getCbCr({
			r: 255,
			g: 0,
			b: 0
		}),
		CHROMAKEY_ZERO: colorspace.getCbCr({
			r: 0,
			g: 0,
			b: 255
		}),

		// these are tolerance, TOLA means totally accurate, TOLB means possible
		// as timecode is ortogonal (red/green/blue) we can be generous here

		TOLA: 50,
		TOLB: 85

	},


	voidfun = () => {
	},
	WITHDEBUG = {
		info: console.error,
		error: console.error,
		log: console.error,
		warn: console.error
	},
	NODEBUG = {
		info: console.error,
		error: voidfun,
		log: voidfun,
		warn: console.error
	};

let

	// set debug level
	debug = NODEBUG;

/**
 * Finds a horizontal line of a given color in a bitmap
 * @param {Bitmap} bitmap
 * @param {number} x
 * @param {number} y
 * @param {TimeCodeOptions} options
 * @return {{}|null}
 */

function findLine(bitmap, x, y, options) {

	const
		color = {},
		{width, height} = bitmap,
		result = {};

	debug.log("from", y, "to", height, "at", x);

	for (let ypos = y; ypos < height; ypos++) {

		bitmap.getPixel(x, ypos, color);

		if (colorspace.isColor(color, options.CHROMAKEY_FRAME, options.TOLA, options.TOLB)) {
			// found target color
			// scan backwards
			debug.log(TAG, "- found color at", x, ypos);
			result.y = ypos;
			for (let ix = x; ix > 0; ix--) {
				bitmap.getPixel(ix, ypos, color);
				if (!colorspace.isColor(color, options.CHROMAKEY_FRAME, options.TOLA, options.TOLB)) {
					result.x0 = ix;
					debug.log(TAG, "- left edge  at", ix, ypos, color);
					// scan forward
					for (ix = x; ix < width; ix++) {
						bitmap.getPixel(ix, ypos, color);
						if (!colorspace.isColor(color, options.CHROMAKEY_FRAME, options.TOLA, options.TOLB)) {
							debug.log(TAG, "- right edge at", ix, ypos, color);
							result.x1 = ix;
							return result;
						}
					}
				}
			}
		} else {
			debug.log("no", color, x, ypos);
		}
	}
	return null;
}


/**
 * Decodes the cropped timecode
 * @param {Bitmap} timecodeBitmap
 * @param {TimeCodeOptions} options
 * @return {Promise<number>} Decoded time value like SS,mmm
 */

async function decodeTimecode(timecodeBitmap, options) {

	const
		{width, height} = timecodeBitmap,
		th = Math.floor(height / 3), // we will look in the third
		// TODO adjust so bitmap only takes the green part !!!
		num = [];

	debug.log("- parse chunk", width, height);

	let current = -1;

	for (let i = 0; i < width; i++) {

		const px = timecodeBitmap.getPixel(i, th);

		if (colorspace.isColor(px, options.CHROMAKEY_ONE, options.TOLA, options.TOLB)) {

			if (current === 1) continue;
			num.push(1);
			current = 1;
			debug.log(TAG, "Found 1");
		} else if (colorspace.isColor(px, options.CHROMAKEY_ZERO, options.TOLA, options.TOLB)) {
			if (current === 0) continue;
			num.push(0);
			current = 0;
			debug.log(TAG, "Found 0");
		} else if (colorspace.isColor(px, options.CHROMAKEY_FRAME, options.TOLA, options.TOLB)) {
			if (current === -1) continue;
			current = -1;
			debug.log(TAG, "Found Bounds");
		} else {
			debug.log(TAG, "Found shit", px);
		}

	}

	if (num.length === 16) {
		const value = parseInt(num.join(""), 2) / 1000;
		debug.log(TAG, "- CORRECT IMAGE:", num, value);
		return value;
	}

	if (options.filename && options.errordir) {
		try {
			await timecodeBitmap.writeFile(options.errordir + "/slice-" + options.slice + "-" + (options.filename.split("/")
				.join("-")));
		} catch (e) {
			debug.error(e);
		}
	}
	throw new Error("Timecode in part " + options.slice + " could not be parsed properly (got only " + num.length + " digits)");


}

/**
 * Scans an image to find the timecode and parses its value.
 * @param {Bitmap} bitmapWithTimecode a bitmap with a timecode generated with http://nebular.tv/timecode
 * @param {TimeCodeOptions} options
 * @return {Promise<number>}
 */

async function scan(bitmapWithTimecode, options) {

	debug.log(TAG, "- processing chunk, size", bitmapWithTimecode.width, "x", bitmapWithTimecode.height);
	// strategy: we scan pixels vertically from the horizontal center until we find a green,
	// that is the border.

	// from the border, we seek left and right until green ends, we get the dimensions;
	// So there is a green line somewhere around the begimm

	// find the green !

	const
		{width} = bitmapWithTimecode,
		finalOptions = {
			...OPTIONS,
			...options
		},
		line = findLine(bitmapWithTimecode, (width / 2), 0, finalOptions);

	if (line) {
		const portion = bitmapWithTimecode.crop({
			top: line.y,
			left: line.x0,
			width: line.x1 - line.x0,
			height: (line.x1 - line.x0) / 10
		});

		return decodeTimecode(portion, finalOptions);
	}

	throw new Error("no line found");

}

/**
 * Scans a number of slices
 * @param {Bitmap[]} chunks
 * @param {TimeCodeOptions?} options
 * @return {ScanResult} decoded values for each slice
 */

async function scanSlices(chunks, options) {

	options = options || {};

	debug = options.debug ? WITHDEBUG : NODEBUG;

	const result = [];
	let i = 0;

	for (const chunk of chunks) {
		// eslint-disable-next-line no-await-in-loop
		result.push(await scan(chunk, {
			...options,
			slice: i++
		}));
	}

	return {
		...options.time && {time: options.time},
		data: result
	};

}

/**
 * Scans an image with 4 timecodes in a grid. Each subimage is the video from a different source,
 * ie. passthrough, edge1, edge2, ... Promise returns the decoded information for each subimage.
 * @param {Bitmap} bitmap The source bitmap
 * @param {TimeCodeOptions} options Scanning options
 * @return {Promise<ScanResult>} Array of decoded values
 */
async function scanQuad(bitmap, options) {

	// bitmap is ready
	const
		// eslint-disable-next-line no-bitwise
		iwidth = bitmap.width >> 1,
		// eslint-disable-next-line no-bitwise
		iheight = bitmap.height >> 1,
		images = [];

	images.push(bitmap.crop({
		top: 0,
		left: 0,
		width: iwidth,
		height: iheight
	}));
	images.push(bitmap.crop({
		top: 0,
		left: iwidth,
		width: iwidth,
		height: iheight
	}));
	images.push(bitmap.crop({
		top: iheight,
		left: 0,
		width: iwidth,
		height: iheight
	}));
	images.push(bitmap.crop({
		top: iheight,
		left: iwidth,
		width: iwidth,
		height: iheight
	}));

	return scanSlices(images, options);
}


/**
 * Scans an image with 2 timecodes in a row. Each subimage is the video from a different source,
 * ie. passthrough, edge1 ... Promise returns the decoded information for each subimage.
 * @param {Bitmap} bitmap The source bitmap
 * @param {TimeCodeOptions} options Scanning options
 * @return {Promise<ScanResult>} Array of decoded values
 */

async function scanDuet(bitmap, options) {

	// bitmap is ready
	const
		// eslint-disable-next-line no-bitwise
		iwidth = bitmap.width >> 1,
		iheight = bitmap.height,
		images = [];

	images.push(bitmap.crop({
		top: 0,
		left: 0,
		width: iwidth,
		height: iheight
	}));
	images.push(bitmap.crop({
		top: 0,
		left: iwidth,
		width: iwidth,
		height: iheight
	}));

	return scanSlices(images, options);
}


/**
 * Scans an image with 2 timecodes in a row. Each subimage is the video from a different source,
 * ie. passthrough, edge1 ... Promise returns the decoded information for each subimage.
 * @param {Bitmap} bitmap The source bitmap
 * @param {TimeCodeOptions} options Scanning options
 * @return {Promise<ScanResult>} Decoded value
 */

async function scanSingle(bitmap, options) {
	const res = await scanSlices([bitmap], options);
	if (res && res[0]) {
		// noinspection JSValidateTypes
		return {
			...options.time && {time: options.time},
			result: res[0]
		};
	}
	throw new Error("unable to decode timecode");
}


/**
 * Loads and scans an image with one, two or four timecode subimages.
 * @param {string} filename
 * @param {number} subImagesPerImage Number of subimages in each screenshot
 * @param {TimeCodeOptions} options
 * @return {Promise<ScanResult>}
 */
async function loadAndScan(filename, subImagesPerImage = 4, options = {}) {

	// read from a file
	const
		time = parseInt(filename.substring(filename.lastIndexOf("-") + 1)
			.split(".")[0], 10),
		bitmap = new imagejs.Bitmap();

	await bitmap.readFile(filename);


	Object.assign(options, {
		filename,
		...Number.isNaN(time) || {time}
	});

	switch (subImagesPerImage) {
		case 4:
			return scanQuad(bitmap, options);
		case 2:
			return scanDuet(bitmap, options);
		case 1:
			return scanSingle(bitmap, options);
		default:
			throw new Error("unsupported image layout, valid values are 1,2,4");
	}
}

/**
 *
 * Scans a list of files
 *
 * @param {String[]} list of files
 * @param {number} subImagesPerImage Number of subimages in each screenshot
 * @param {TimeCodeOptions} options
 * @return {Promise<ScanResult[]>}
 */

async function loadAndScanList(list, subImagesPerImage = 4, options = {}) {

	options = options || {};

	const
		results = [],
		parts = "\\|/-",
		SKIP = options.SKIP || 0;

	if (list.length < SKIP + 1) {
		return null;
	}

	let ko = 0;

	for (let i = SKIP; i < list.length; i++) {
		const val = list[i];
		if (val.endsWith(".png") || val.endsWith(".jpg")) {
			try {
				// eslint-disable-next-line
				const result = await loadAndScan(val, subImagesPerImage, options);
				results.push({image: val, ...result});

				if (options.progress) {
					process.stderr.write(
						"\r" + parts[i % 4] + " Scanned " + (i - SKIP) + "/" + (list.length - SKIP) + ": " + val.substring(val.lastIndexOf("/")
						+ 1) + ": " + result.data + "                                "
					);
				}

			} catch (e) {
				if (options.progress) process.stderr.write("\r! Image " + val + " error: " + e.message + "\n");
				ko++;
			}
		} else {
			debug.warn("! invalid image name", val, "image skipped.");
			ko++;
		}
	}

	if (options.progress) process.stderr.write("\r- Finished scanning all images, discarded " + ko + " images.                                                \n");

	results.sort((i1, i2) => i1.time - i2.time);

	return (options.parser && typeof options.parser === "function")
		? options.parser(results)
		: results;

}

/**
 *
 * Decodes all images in a directory
 *
 * @param {string} directory
 * @param {number} subImagesPerImage Number of subimages with timecodes in a snapshot: 1, 2 or 4
 * @param {TimeCodeOptions} options TimeCode algorythm options
 * @param {Object} properties Raw properties to copy to the result object
 * @return {Promise<ScanResult[]>}
 */

function loadAndScanDir(directory, subImagesPerImage, options, properties = null) {

	return new Promise((resolve, reject) => {

		fs.readdir(directory, async (err, files) => {

			if (err) {

				reject(err);

			} else if (files.length) {

				let mintime = Number.MAX_VALUE,
					maxtime = 0;

				const newFiles = files.map(
					(file) => {

						const
							time = parseInt(file.substring(file.lastIndexOf("-") + 1)
								.split(".")[0], 10);

						if (!Number.isNaN(time) && time > 1000000) {
							// got a file with a timestamp
							if (time < mintime) mintime = time;
							if (time > maxtime) maxtime = time;
						}

						return directory + "/" + file;

					}
				);

				try {
					resolve(
						{
							test: {
								...(properties || {}),
								maxtime,
								mintime,
								samples: files.length
							},
							...await loadAndScanList(
								newFiles,
								subImagesPerImage,
								{
									progress: true,
									debug: false,
									...options
								}
							)
						}
					);
				} catch (e) {
					reject(e);
				}

			} else {
				console.error("! No files found in", directory);
			}
		});
	});
}


// noinspection JSUnusedGlobalSymbols
module.exports = {
	scan,
	scanQuad,
	scanDuet,
	scanSingle,
	loadAndScan,
	loadAndScanList,
	loadAndScanDir
};
