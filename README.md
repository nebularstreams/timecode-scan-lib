# timecode-scan-lib #

A NodeJS library to scan screenshots with digital timecodes, generated with the timecode OBS plugin page 
 https://nebular.tv/timecode.
 
Screenshots can have 1, 2 or 4 video windows in a grid (see template folder). The Library provides functions
to scan all these different layouts:

- "Duet", compares two video windows, typically local and user.
- "Quad", compares four video windows, typically local and 3 different transcoded streams.
- "Single", parses a single video window timecode.


### Installation ###

Just add the library to your project:

```npm install -save timecode-scan-lib```

### Usage ###

```async function loadAndScan(filename, subImagesPerImage = 4, options = {}) ```

 * Loads and scans an image with one, two or four timecode subimages.
 * ```filename``` (string) Path to an existing screenshot
 * ```subImagesPerImage``` (number) 1, 2 or 4 images (video windows) per screenshot
 * ```options``` (TimeCodeOptions) Assorted options

```async function scanQuad(bitmap, options)```

 * Scans an image with 4 timecodes in a grid. Each subimage is the video from a different source, ie. passthrough, edge1, edge2, ... Promise returns the decoded information for each subimage.
 * ```bitmap```@param {imagejs.Bitmap} bitmap The source bitmap
 * ```options``` scanning otions
 * return ```Promise<number[]>``` An Array of decoded values

```async function scanDuet(bitmap, options)```

 * Scans an image with 2 timecodes in a grid. Each subimage is the video from a different source, ie. passthrough, edge1, edge2, ... Promise returns the decoded information for each subimage.
 * ```bitmap```@param {imagejs.Bitmap} bitmap The source bitmap
 * ```options``` scanning otions
 * return ```Promise<number[]>``` An Array of decoded values

```async function scanSingle(bitmap, options)```

 * Scans an image with a timecodes. Returns the parsed time value.
 * ```bitmap```@param {imagejs.Bitmap} bitmap The source bitmap
 * ```options``` scanning otions
 * return ```Promise<number>``` Decoded time value
 

### License
(C) 2020 Nebular Streams, License CC-BY-SA
